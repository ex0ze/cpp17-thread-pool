#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <type_traits>
#include <functional>
#include <thread>
#include <queue>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <future>

template <std::size_t numThreads>
class ThreadPool
{
public:
    enum class RAIIPolicy { Join, Detach };
    ThreadPool(RAIIPolicy policy = RAIIPolicy::Join) : m_stop(false), m_destructionPolicy(policy)
    {
        for (auto& t : m_threads)
        {
            t = std::thread{[this](){
                while (!m_stop.load())
                {
                    std::unique_lock<decltype (m_readWriteLock)> locker(m_readWriteLock);
                    m_taskNotifier.wait(locker, [this]{return !m_tasks.empty();});
                    auto task = std::move(m_tasks.front());
                    m_tasks.pop_front();
                    locker.unlock();
                    task();
                }
            }};
        }
    }
    template <typename Func, typename ... Args, typename Ret = std::invoke_result_t<Func, Args...>>
    std::future<Ret> submitTask(Func && f, Args&&...args)
    {
        auto res_promise = std::make_shared<std::promise<Ret>>();
        auto res_future (res_promise->get_future());
        {
            std::lock_guard<decltype (m_readWriteLock)> locker(m_readWriteLock);
            m_tasks.emplace_back([invoker = (std::bind(std::forward<Func>(f), std::forward<Args>(args)...)), promise = std::move(res_promise)]()
            {
                if constexpr (!std::is_void_v<Ret>) //! workaround for void - non void functions
                        promise->set_value(invoker());
                else
                {
                    invoker();
                    promise->set_value();
                }
            });
        }
        m_taskNotifier.notify_one();
        return res_future;
    }
    ~ThreadPool()
    {
        m_stop.store(true);
        for (auto& t : m_threads)
        {
            if (m_destructionPolicy == RAIIPolicy::Join)
            {
                if (t.joinable())
                    t.join();
            }
            else
            {
                t.detach();
            }

        }
    }
private:
    std::array<std::thread, numThreads>     m_threads;
    std::deque<std::function<void()>>       m_tasks;
    std::mutex                              m_readWriteLock;
    std::condition_variable                 m_taskNotifier;
    std::atomic<bool>                       m_stop;
    RAIIPolicy                              m_destructionPolicy;
};

#endif // THREADPOOL_H
