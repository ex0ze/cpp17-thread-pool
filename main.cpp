#include <iostream>
#include <random>
#include <chrono>
#include "threadpool.hpp"


int main()
{
    using namespace std::chrono_literals;
    ThreadPool<4> pool;
    for (int i = 0; i < 3; ++i) //! let's add some fun tasks
        pool.submitTask([i] {
        while (true) {
            std::cout << "Hello from thread " << std::this_thread::get_id() << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(i + 1));
        }
    });
    std::vector<int> vec;
    constexpr auto numElements = 1'000'000;
    std::uniform_int_distribution<> dist(-1'000'000, 1'000'000);
    std::mt19937 engine {};
    std::generate_n(std::inserter(vec, std::end(vec)), //! fill vector with random elements from -1M to 1M
                    numElements,
                    [dist = std::move(dist), gen = std::move(engine)]() mutable { return dist(gen); }
    );
    auto sorted_state = pool.submitTask([&](const decltype (vec)& container)
    { return std::is_sorted(container.cbegin(), container.cend()); }, std::cref(vec)).get(); //! check if vec is sorted in another thread

    std::cout << "Generated array with " << numElements << " elements. Array is sorted? " << std::boolalpha << sorted_state << "\nStarting sort task..." << std::endl;

    auto sorting_task = [](auto& container) { std::sort(std::begin(container), std::end(container)); };
    auto f = pool.submitTask(std::bind(std::move(sorting_task), std::ref(vec)));
    f.get();

    std::cout << "done. Array is sorted? " << std::boolalpha << std::is_sorted(std::cbegin(vec), std::cend(vec)) << std::endl;
    return 0;
}
